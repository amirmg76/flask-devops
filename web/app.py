from pymongo import MongoClient
from OpenSSL import SSL
from flask import Flask, jsonify, request

app = Flask(__name__)

# context = SSL.Context(SSL.OPENSSL_VERSION)
# context.use_privatekey_file('server.key')
# context.use_certificate_file('server.crt')

# app.config['ENV'] = 'develpment'
# app.config['DEBUG'] = True

# def get_collection():
#     client = MongoClient('mongodb://localhost:27017/')
#     db = client['test_db']
#     collection = db['jsons']
#     return collection

# collection = get_collection()

# @app.route('/add', methods=['POST'])
# def add():
#     data = request.get_json()
#     id_number = collection.find().count() + 1
#     data['id'] = id_number
#     collection.insert_one(data)
#     return 'hey'

# @app.route('/find/<int:id>')
# def find(id):
#     try:
#         data = collection.find_one({'id': id})
#         del data['_id']
#         return jsonify(data)
#     except TypeError:
#         return '<h1>ERROR 404</h1><br/>there is no data with this id', 404

# @app.route('/find/list')
# def json_list():
#     try:
#         new_json = []
#         data = collection.find()
#         for doc in data:
#             print(doc)
#             del doc['_id']
#             new_json.append(doc)
#         return jsonify(new_json)
#     except TypeError:
#         return 'there is no json file', 404

@app.route('/')
def index():
    return 'hello'

if __name__ == '__main__':
    context = ('local.crt', 'local.key')
    app.run(host='amg76.heroku.com', port='80', ssl_context=context)